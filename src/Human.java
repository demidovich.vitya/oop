 enum sex{
    woman,
    man
} ;
public abstract class Human<virtual> {
   private String name;
   private int age;
   private float weight;
   private sex sex;
   private static int count = 0;

   public Human(String name, int age, float weight, sex sex)
    {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.sex = sex;
        count++;
    }
   public String getName()
    {
        return this.name;
    }
    public sex getSex()
    {
        return this.sex;
    }

    public void setName(String name)
    {
        this.name=name;
    }

    abstract void work();

   public static int getCountHuman(){
       return count;
   }

}


