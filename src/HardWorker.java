public class HardWorker extends Human  {
    private  String profession;
    HardWorker(String name, int age, float weight, sex sex, String profession){
        super(name, age, weight, sex);
        this.profession = profession;
    }
    @Override
    void work() {
        System.out.println(super.getName() + " работяга на " + profession + "е");
    }

    public void timeForWorking(){
        System.out.println(super.getName() + " работает опять");
    }


}
