public class Streamer extends Human {
    private int subscribe;
    Streamer(String name, int age, float weight, sex sex, int subscribe){
        super(name, age, weight, sex);
        this.subscribe = subscribe;
    }
    @Override
    void work() {
        System.out.println(super.getName() + " стримлю для " + subscribe + " человек");
    }

    public void baitForDonate(){
        System.out.println(super.getName() + ": даб даб даб я ");
    }
}
